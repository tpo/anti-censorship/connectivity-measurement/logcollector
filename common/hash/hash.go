package hash

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
)

func GetFileHash(path string) (string, error) {
	fd, err := os.Open(path)
	if err != nil {
		return "", err
	}

	sha256hasher := sha256.New()
	_, err = io.Copy(sha256hasher, fd)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(sha256hasher.Sum(nil)), nil
}
