package filekv

import (
	"errors"
	"os"
	"path/filepath"
)

func NewFileKV(basePath string) *FileKV {
	return &FileKV{
		basePath: basePath,
	}
}

type FileKV struct {
	basePath string
}

func (f *FileKV) Write(key string, value []byte) error {
	appendixPath := filepath.Dir(key)
	err := os.MkdirAll(f.basePath+appendixPath, 0755)
	if err != nil {
		return err
	}
	err = os.WriteFile(f.basePath+key, value, 0644)
	if err != nil {
		return err
	}
	return nil
}

func (f *FileKV) Read(key string) ([]byte, error) {
	return os.ReadFile(f.basePath + key)
}

func (f *FileKV) Delete(key string) error {
	return os.Remove(f.basePath + key)
}

func (f *FileKV) ListObj(key string) ([]Entry, error) {
	appendixPath := filepath.Base(key)
	dir, err := os.ReadDir(f.basePath + appendixPath)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, err
	}
	var entries []Entry
	for _, entry := range dir {
		entries = append(entries, Entry{
			isDir: entry.IsDir(),
			name:  entry.Name(),
		})
	}
	return entries, nil
}

type Entry struct {
	isDir bool
	name  string
}

func (e Entry) IsDir() bool {
	return e.isDir
}

func (e Entry) Name() string {
	return e.name
}
