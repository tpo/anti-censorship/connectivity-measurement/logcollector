package protokv

import (
	"github.com/xiaokangwang/torprobeutil/common/filekv"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

type ProtoKV struct {
	*filekv.FileKV
}

func NewProtoKV(basePath string) *ProtoKV {
	return &ProtoKV{FileKV: filekv.NewFileKV(basePath)}
}

func (p *ProtoKV) WriteProto(key string, value proto.Message) error {
	result := protojson.Format(value)
	return p.Write(key, []byte(result))
}

func (p *ProtoKV) ReadProto(key string, value proto.Message) error {
	result, err := p.Read(key)
	if err != nil {
		return err
	}
	return protojson.Unmarshal(result, value)
}
