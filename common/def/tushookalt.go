package def

import "google.golang.org/grpc"

var HookHandler_ServiceDescAlt = grpc.ServiceDesc{
	ServiceName: "v2.HookHandler",
	HandlerType: (*HookHandlerServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "InvokeHook",
			Handler:    _HookHandler_InvokeHook_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "common/def/tushook.proto",
}

func RegisterHookHandlerServerAlt(s grpc.ServiceRegistrar, srv HookHandlerServer) {
	s.RegisterService(&HookHandler_ServiceDescAlt, srv)
}
