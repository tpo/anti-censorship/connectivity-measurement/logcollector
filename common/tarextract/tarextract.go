package tarextract

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"errors"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/xiaokangwang/torprobeutil/common/hash"
	"github.com/xiaokangwang/torprobeutil/common/testResultCollector"
)

func NewTarExtractor(collector testResultCollector.Collector) testResultCollector.Extractor {
	return &tarExtractor{collector: collector}
}

type tarExtractor struct {
	collector testResultCollector.Collector
}

func (e *tarExtractor) Extract(path string) error {
	fileHash, err := hash.GetFileHash(path)
	if err != nil {
		return err
	}
	tarGzFile, err := os.Open(path)
	if err != nil {
		return err
	}
	gzFile, err := gzip.NewReader(tarGzFile)
	if err != nil {
		return err
	}
	tarStructure := tar.NewReader(gzFile)
	for {
		header, err := tarStructure.Next()
		if err != nil {
			if errors.Is(err, io.EOF) {
				return nil
			}
			return err
		}
		name := header.Name
		filename := filepath.Base(name)

		if strings.HasSuffix(filename, ".log") && !strings.HasSuffix(filename, "client.log") {
			machine := strings.TrimSuffix(filename, ".log")
			scanner := bufio.NewScanner(tarStructure)
			percentage := ""
			initTime := time.Time{}
			endTime := time.Time{}
			for scanner.Scan() {
				content := scanner.Text()
				exp := regexp.MustCompile("([^\\[]*) [^ ]* Bootstrapped ([0-9]+)%")
				if matched := exp.FindStringSubmatch(content); matched != nil {
					timestamp := ""
					timestamp = matched[1]
					percentage = matched[2]
					timestampAsTime, err := ParseTorTimeStamp(timestamp)
					if err != nil {
						return err
					}
					switch percentage {
					case "0":
						initTime = timestampAsTime
					case "100":
						endTime = timestampAsTime
					}
				}
			}

			out, _ := strconv.ParseInt(percentage, 10, 32)
			e.collector.OnCollectedBootstrapPercentage(machine, int(out))
			if out == 100 {
				e.collector.OnCollectedBootstrapTime(machine, endTime.Sub(initTime).Seconds())
			}
			e.collector.OnCollectedSourceHash(machine, fileHash)
		}
	}

}

func ParseTorTimeStamp(timestamp string) (time.Time, error) {
	return time.Parse("Jan 02 15:04:05.999", timestamp)
}
