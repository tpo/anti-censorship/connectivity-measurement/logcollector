package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/eventials/go-tus"
	"github.com/eventials/go-tus/memorystore"
	"github.com/xiaokangwang/torprobeutil/common/bridgelineextract"
	"github.com/xiaokangwang/torprobeutil/common/def"
	hash2 "github.com/xiaokangwang/torprobeutil/common/hash"
	"github.com/xiaokangwang/torprobeutil/common/tarextract"
	"github.com/xiaokangwang/torprobeutil/common/testResultCollector"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/protobuf/encoding/protojson"
)

func main() {
	tarIn := flag.String("in-tar", "", "")
	bridgeLineIn := flag.String("in-bridgeline", "", "")
	dataOut := flag.String("out-type", "", "")
	testSiteName := flag.String("test-site-name", "", "")
	grpcTarget := flag.String("grpc-target", "", "")
	grpcAuthority := flag.String("grpc-authority", "", "")
	hashStoreTarget := flag.String("hashstore-target", "", "")
	action := flag.String("action", "submit", "")
	flag.Parse()
	switch *action {
	case "submit":
		logCollector := testResultCollector.NewLogCollector()
		if *bridgeLineIn != "" {
			bridgelineextract.NewBridgeLineExtractor(logCollector).Extract(*bridgeLineIn)
		}
		if *tarIn != "" {
			tarextract.NewTarExtractor(logCollector).Extract(*tarIn)
		}
		data := logCollector.GetData()
		for _, d := range data {
			d.TestSite = *testSiteName
		}
		switch *dataOut {
		case "line":
			for _, d := range data {
				fmt.Println(d.TestName, " ", d.Detail.BootstrapPercentage)
			}
		case "jsonl":
			for _, d := range data {
				data, err := protojson.Marshal(d)
				if err != nil {
					continue
				}
				fmt.Println(string(data))
			}
		case "grpc":

			clientConn := getGRPC(grpcTarget, grpcAuthority)

			probeTelemetryClient := def.NewProbeTelemetryServiceClient(clientConn)

			var dataAsArray []*def.Summary
			for _, d := range data {
				dataAsArray = append(dataAsArray, d)
			}
			_, err := probeTelemetryClient.SubmitProbeResult(context.TODO(), &def.SubmitProbeResultReq{Summary: dataAsArray})
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	case "update":
		clientConn := getGRPC(grpcTarget, grpcAuthority)
		probeTelemetryClient := def.NewProbeTelemetryServiceClient(clientConn)
		resp, err := probeTelemetryClient.GetProbeConfig(context.TODO(), &def.GetConfigFileReq{SiteName: *testSiteName})
		if err != nil {
			fmt.Println(err.Error())
		}
		for _, v := range resp.Configs {
			err := os.WriteFile(v.Name, []byte(v.Content), 0600)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	case "uploadhashed":
		fileHash, err := hash2.GetFileHash(*tarIn)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}
		for s := 0; s <= 5; s++ {
			fd, err := os.Open(*tarIn)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(0)
			}
			req, err := http.NewRequest("PUT", *hashStoreTarget+"/upload", fd)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(0)
			}
			req.Header.Add("Expected-Hash", fileHash)
			resp, err := http.DefaultTransport.RoundTrip(req)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			if resp.StatusCode != 200 {
				fmt.Println("unsuccessful upload")
				continue
			}
			break
		}
	case "uploadhashedChunked":
		fileHash, err := hash2.GetFileHash(*tarIn)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}

		f, err := os.Open(*tarIn)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}

		tusConfig := tus.DefaultConfig()
		tusConfig.Resume = true
		tusConfig.Store, _ = memorystore.NewMemoryStore()

		// create the tus client.
		client, err := tus.NewClient(*hashStoreTarget, tusConfig)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}

		// create an upload from a file.
		upload, err := tus.NewUploadFromFile(f)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}

		upload.Metadata["filename"] = fileHash

		for retryCount := 0; retryCount <= 128; retryCount++ {
			// create the uploader.
			uploader, err := client.CreateOrResumeUpload(upload)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}

			// start the uploading process.
			err = uploader.Upload()
			if err != nil {
				fmt.Println(err.Error())
				time.Sleep(time.Second)
				continue
			}
			break
		}
	}

}

func getGRPC(grpcTarget, authority *string) *grpc.ClientConn {
	cert, err := tls.LoadX509KeyPair("client.pem", "client.key")
	if err != nil {
		fmt.Printf("load key: %s", err)
	}

	certPEMBlock, err := os.ReadFile("ca.pem")
	if err != nil {
		fmt.Printf("load ca: %s", err)
	}
	caCertByte, _ := pem.Decode(certPEMBlock)
	caCert, err := x509.ParseCertificate(caCertByte.Bytes)
	if err != nil {
		fmt.Printf("load ca: %s", err)
	}
	pool := x509.NewCertPool()
	pool.AddCert(caCert)
	config := &tls.Config{Certificates: []tls.Certificate{cert}, RootCAs: pool, NextProtos: []string{"h2"}}

	effectiveAuthority := *authority
	if effectiveAuthority == "" {
		host, _, err := net.SplitHostPort(*grpcTarget)
		if err != nil {
			panic(err)
		}
		effectiveAuthority = host
	}

	clientConn, err := grpc.Dial(*grpcTarget, grpc.WithAuthority(effectiveAuthority), grpc.WithTransportCredentials(credentials.NewTLS(config)))

	if err != nil {
		fmt.Printf("create grpc connection: %s", err)
	}
	return clientConn
}
