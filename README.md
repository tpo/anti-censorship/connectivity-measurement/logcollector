#Log Collector

Log Collector receives data from various vantage points
in different locations. The data are sent in the form of mTLS gRPC
requests in the format defined in `def/summary.proto`.

Additionally, Log Collector can distribute the probe configuration to
the clients. This will allow testing targets to be remotely configured.