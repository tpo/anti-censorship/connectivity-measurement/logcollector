package fileserver

import (
	"fmt"
	"net/http"
	"os"
)

type HTTPService interface {
	InitHTTP(mux *http.ServeMux) error
}

func NewHashedDownloader(pathBase string) HTTPService {
	return &downloader{pathBase: pathBase}
}

func mustGetConfFromEnv(name string) string {
	data, ok := os.LookupEnv(name)
	if ok {
		return data
	}
	panic(fmt.Sprintf("no env %v", name))
}

func NewHashedDownloaderFromEnvironment() HTTPService {
	return NewHashedDownloader(mustGetConfFromEnv("HASHED_STORAGE_BASE"))
}

type downloader struct {
	pathBase string
}

func (d *downloader) InitHTTP(mux *http.ServeMux) error {
	mux.Handle("/download/", http.StripPrefix("/download/", http.FileServer(http.Dir(d.pathBase))))
	return nil
}
