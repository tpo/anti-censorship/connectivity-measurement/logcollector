package tusuploader

import (
	"context"
	"errors"
	"os"

	"github.com/xiaokangwang/torprobeutil/common/def"
	hash2 "github.com/xiaokangwang/torprobeutil/common/hash"
)

type tusUploader struct {
	dataDir string

	def.UnsafeHookHandlerServer
}

func (t *tusUploader) InvokeHook(ctx context.Context, request *def.HookRequest) (*def.HookResponse, error) {
	var reterr error
	switch request.Type {
	case "post-finish":
		if request.Event.Upload.MetaData == nil {
			break
		}
		expectedHash := request.Event.Upload.MetaData["filename"]
		dataPath := t.dataDir + "/" + request.Event.Upload.Id
		actualHash, err := hash2.GetFileHash(dataPath)
		if err != nil {
			reterr = err
			break
		}
		if actualHash != expectedHash {
			reterr = errors.New("content mismatch")
			break
		}
		err = os.Link(dataPath, t.dataDir+"/"+expectedHash)
		if err != nil {
			reterr = err
			break
		}
	}
	return &def.HookResponse{}, reterr
}

func NewTusUploaderProcessor(dataDir string) def.HookHandlerServer {
	return &tusUploader{dataDir: dataDir}
}
