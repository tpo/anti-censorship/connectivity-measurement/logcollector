package simplefileuploader

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base32"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type HTTPService interface {
	InitHTTP(mux *http.ServeMux) error
}

func NewHashedUploader(pathBase string) HTTPService {
	return &uploader{pathBase: pathBase}
}

func mustGetConfFromEnv(name string) string {
	data, ok := os.LookupEnv(name)
	if ok {
		return data
	}
	panic(fmt.Sprintf("no env %v", name))
}

func NewHashedUploaderFromEnvironment() HTTPService {
	return NewHashedUploader(mustGetConfFromEnv("HASHED_STORAGE_BASE"))
}

type uploader struct {
	pathBase string
}

func (u *uploader) InitHTTP(mux *http.ServeMux) error {
	mux.HandleFunc("/upload", u.upload)
	return nil
}

func randString() string {
	var randData [16]byte
	rand.Read(randData[:])
	return base32.StdEncoding.EncodeToString(randData[:])
}

func (u *uploader) upload(writer http.ResponseWriter, request *http.Request) {
	intentedHash := request.Header.Get("Expected-Hash")
	decodedHash, err := hex.DecodeString(intentedHash)
	if err != nil {
		writer.WriteHeader(400)
		log.Println(err.Error())
		return
	}

	if len(decodedHash) != 32 {
		writer.WriteHeader(400)
		log.Println("invalid hash len")
		return
	}

	reqBody := request.Body
	if err != nil {
		writer.WriteHeader(400)
		log.Println(err.Error())
		return
	}
	fileName := u.pathBase + "." + randString()
	fd, err := os.Create(fileName)
	if err != nil {
		writer.WriteHeader(400)
		log.Println(err.Error())
		return
	}
	hasher := sha256.New()
	reader := io.TeeReader(reqBody, hasher)
	io.Copy(fd, reader)
	fd.Close()
	reqBody.Close()
	contentHash := hasher.Sum(nil)
	if hmac.Equal(contentHash, decodedHash) {
		os.Rename(fileName, u.pathBase+hex.EncodeToString(decodedHash))
		return
	}
	writer.WriteHeader(400)
	log.Println("hash not match")
	os.Remove(fileName)
	return
}
