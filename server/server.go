package main

import (
	"fmt"
	"github.com/xiaokangwang/torprobeutil/common/protokv"
	"github.com/xiaokangwang/torprobeutil/server/summary"
	"github.com/xiaokangwang/torprobeutil/server/summary/processor/prometheusout"
	"net/http"
	"os"

	"github.com/xiaokangwang/torprobeutil/server/fileserver"
	"github.com/xiaokangwang/torprobeutil/server/grpcd"
	"github.com/xiaokangwang/torprobeutil/server/simplefileuploader"

	_ "github.com/xiaokangwang/torprobeutil/server/summary/processor/jsonllog"
)

func main() {
	ServeMux := http.NewServeMux()
	downloadServer := fileserver.NewHashedDownloaderFromEnvironment()
	downloadServer.InitHTTP(ServeMux)
	uploadServer := simplefileuploader.NewHashedUploaderFromEnvironment()
	uploadServer.InitHTTP(ServeMux)

	go http.ListenAndServe(mustGetConfFromEnv("HTTP_LISTEN_ADDR"), ServeMux)
	go grpcd.StartPrivateServer()

	kvdir := mustGetConfFromEnv("KVDIR")
	protokv := protokv.NewProtoKV(kvdir)
	promeOutputter := prometheusout.NewPrometheusOutputter(protokv)
	summary.RegisterResultCollector("prometheus", promeOutputter)

	grpcd.StartServer()
}

func mustGetConfFromEnv(name string) string {
	data, ok := os.LookupEnv(name)
	if ok {
		return data
	}
	panic(fmt.Sprintf("no env %v", name))
}
