package prometheusout

import (
	"context"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/xiaokangwang/torprobeutil/common/def"
	"github.com/xiaokangwang/torprobeutil/common/protokv"
	"net/http"
)

type PrometheisOutputter struct {
	storage *protokv.ProtoKV

	vantagePointLastSeen *prometheus.GaugeVec
}

func (p *PrometheisOutputter) SubmitProbeResult(ctx context.Context, req *def.SubmitProbeResultReq) error {
	for _, value := range req.Summary {
		p.vantagePointLastSeen.WithLabelValues(value.TestSite).Set(float64(value.ServerReportReceivingTime))
		vp := &def.KnownVantagePoint{}
		p.storage.ReadProto("vantagepoints/"+value.TestName, vp)

		vp.LastSeen = value.ServerReportReceivingTime
		err := p.storage.WriteProto("vantagepoints/"+value.TestSite, vp)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *PrometheisOutputter) restoreFromStorage() {
	entries, err := p.storage.ListObj("vantagepoints")
	if err != nil {
		panic(err)
	}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		vp := &def.KnownVantagePoint{}
		err = p.storage.ReadProto("vantagepoints/"+entry.Name(), vp)
		if err != nil {
			panic(err)
		}
		p.vantagePointLastSeen.WithLabelValues(entry.Name()).Set(float64(vp.LastSeen))
	}
}

func (p *PrometheisOutputter) register(registry *prometheus.Registry) {
	p.vantagePointLastSeen = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "torprobe_vantagepoint_lastseen",
		Help: "The last seen time of the vantage point",
	}, []string{"vantage_point"})

	err := registry.Register(p.vantagePointLastSeen)
	if err != nil {
		panic(err)
	}
}
func (p *PrometheisOutputter) init() {
	// Create a non-global registry.
	reg := prometheus.NewRegistry()

	p.register(reg)
	p.restoreFromStorage()

	httpMux := http.NewServeMux()
	httpMux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}))
	panic(http.ListenAndServe("127.0.0.1:8080", httpMux))
}

func NewPrometheusOutputter(storage *protokv.ProtoKV) *PrometheisOutputter {
	p := &PrometheisOutputter{storage: storage}
	go p.init()
	return p
}
